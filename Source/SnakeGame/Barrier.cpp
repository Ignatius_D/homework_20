// Fill out your copyright notice in the Description page of Project Settings.


#include "Barrier.h"
#include "BarrierElement.h"
#include "SnakeBase.h"

// Sets default values
ABarrier::ABarrier()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BlockSize=100.f;
	StartPosX = -500.f;
	StartPosY = -1000.f;
	SizeX=10;
	SizeY=20;
}

// Called when the game starts or when spawned
void ABarrier::BeginPlay()
{
	Super::BeginPlay();
	//AddBarrierElements();
}

// Called every frame
void ABarrier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABarrier::AddBarrierElements()
{
	for (int i = 0; i < SizeX; ++i) {
		for (int k = 0; k < SizeY; ++k) {
			if (i == 0 || i == SizeX - 1||k==0||k==SizeY-1) {
				FVector NewLocation(StartPosX + i * BlockSize, StartPosY + k * BlockSize, 0);
				FTransform NewTransform(NewLocation);
				ABarrierElement* NewBarrierElem = GetWorld()->SpawnActor<ABarrierElement>(BarrierElementClass, NewTransform);
				NewBarrierElem->BarrierOwner = this;
				BarrierElements.Add(NewBarrierElem);
			}
		}
	}
}
